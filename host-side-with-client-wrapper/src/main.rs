// Import the wasmer runtime so we can use it
use wasmer_runtime::{error, func, imports, instantiate, Array, Ctx, Func, WasmPtr};
use std::ptr;
use std::os::raw::c_void;
use std::ops::{Deref, DerefMut};

/// Zero-sized struct used as an opaque ptr
#[derive(Copy, Clone)]
pub struct MemoryBlock {
    _unused: [u8; 0],
}

struct Data<'a> {
    memory_alloc: Func<'a, u32, WasmPtr<MemoryBlock>>,
    memory_ptr: Func<'a, WasmPtr<MemoryBlock>, WasmPtr<u8, Array>>,
    memory_free: Func<'a, WasmPtr<MemoryBlock>>,
}

fn free_data(data: *mut c_void) {
    unsafe {
        ptr::drop_in_place(data as *mut Data);
    }
}

// The struct that stores all the data.
#[derive(Default, Clone, Debug)]
struct User {
    name: String,
}

#[derive(Default, Clone, Debug)]
struct Users(Vec<User>);

impl Drop for Users {
    fn drop(&mut self) {
        println!("Dropping Users {:?}", self);
    }
}

impl Drop for User {
    fn drop(&mut self) {
        println!("Dropping User {:?}", self);
    }
}

impl Deref for Users {
    type Target = Vec<User>;

    fn deref(&self) -> &Vec<User> {
        &self.0
    }
}

impl DerefMut for Users {
    fn deref_mut(&mut self) -> &mut Vec<User> {
        &mut self.0
    }
}

fn users_new() -> u64 {
    Box::into_raw(Box::new(Users::default())) as u64
}

fn users_free(users: u64) {
    unsafe {
        ptr::drop_in_place(users as *mut Users);
    }
}

fn users_len(users: u64) -> u64 {
    let users = unsafe {
        &*(users as *const Users)
    };
    users.len() as u64
}

fn users_add(users: u64, user: u64) {
    let users = unsafe {
        &mut *(users as *mut Users)
    };
    let user = unsafe {
        ptr::read(user as *const User)
    };
    users.push(user);
}

/// Get the user handle (pointer) from the input index.
/// This is mutably borrowed.
fn users_get_user(users: u64, user: u64) -> u64 {
    let users = unsafe {
        &mut *(users as *mut Users)
    };
    let user = unsafe {
        match users.get_mut(user as usize) {
            Some(u) => u,
            None => return 0,
        }
    };
    user as *mut User as u64
}

fn user_new() -> u64 {
    Box::into_raw(Box::new(User::default())) as u64
}

fn user_free(user: u64) {
    unsafe {
        ptr::drop_in_place(user as *mut User);
    }
}

fn user_set_name(ctx: &mut Ctx, user: u64, name: WasmPtr<u8, Array>, name_len: u32) {
    let user = unsafe { &mut *(user as *mut User) };
    let memory = ctx.memory(0);
    let string = name.get_utf8_string(memory, name_len).unwrap();
    user.name = string.into();
}

fn user_get_name(ctx: &mut Ctx, user: u64) -> WasmPtr<MemoryBlock> {
    let user = unsafe { &*(user as usize as *mut User) };
    let memory = ctx.memory(0);
    let data: &Data = unsafe {
        &*(ctx.data as *mut Data)
    };
    let block = data.memory_alloc.call(user.name.len() as u32).unwrap();
    let ptr = data.memory_ptr.call(block).unwrap();
    let range = ptr.deref(memory, 0, user.name.len() as u32).unwrap();
    // Set the string
    for (byte, cell) in user.name.bytes().zip(range.into_iter()) {
        cell.set(byte);
    }
    block
}

fn print(ctx: &mut Ctx, s: WasmPtr<u8, Array>, len: u32) {
    let memory = ctx.memory(0);
    let string = s.get_utf8_string(memory, len).unwrap();
    print!("{}", string);
}

fn main() -> error::Result<()> {
    let wasm_bytes = include_bytes!("../module/target/wasm32-unknown-unknown/release/module.wasm");

    let import_object = imports! {
        "host" => {
            "users_new" => func!(users_new),
            "users_len" => func!(users_len),
            "users_free" => func!(users_free),
            "users_add" => func!(users_add),
            "users_get_user" => func!(users_get_user),
            "user_new" => func!(user_new),
            "user_free" => func!(user_free),
            "user_set_name" => func!(user_set_name),
            "user_get_name" => func!(user_get_name),
            "print" => func!(print),
        },
    };
    let mut instance = instantiate(wasm_bytes, &import_object)?;

    let memory_alloc: Func<u32, WasmPtr<MemoryBlock>> = instance.func("memory_alloc")?;
    let memory_ptr: Func<WasmPtr<MemoryBlock>, WasmPtr<u8, Array>> = instance.func("memory_ptr")?;
    let memory_free: Func<WasmPtr<MemoryBlock>> = instance.func("memory_free")?;
    let data = Data{
        memory_alloc,
        memory_ptr,
        memory_free,
    };
    let data = Box::into_raw(Box::new(data)) as *mut c_void;
    instance.context_mut().data = data;
    instance.context_mut().data_finalizer = Some(free_data);

    let main: Func = instance.func("main")?;
    main.call()?;

    Ok(())
}
