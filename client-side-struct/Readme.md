# client-side-struct

This example is a struct created, managed, and allocated on the client side.
It is worked with from the host side via a C-style interface and an opaque
pointer.

This is similar to the host-side-struct example, but with the struct managed on
the other side of it.

We have to do an allocation dance here to pass a string of arbitrary size into
wasm.  We could also use a static memory block, or a static resizable vector,
but this way scales better and also demonstrates a bit of how dynamic memory
management can be done with WASM interaction.
We can also use std::alloc instead of a vector for allocation by creating a
MemoryBlock class to store layout data and such.  A vec is easier.
