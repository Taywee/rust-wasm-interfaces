// Import the wasmer runtime so we can use it
use wasmer_runtime::{error, imports, instantiate, Array, Func, WasmPtr, func, Ctx};
use std::cell::Cell;
use shared_capnp_struct::foo_capnp;

/// Zero-sized struct used as an opaque ptr
#[derive(Copy, Clone)]
pub struct MemoryBlock {
    _unused: [u8; 0],
}

fn foo_print(ctx: &mut Ctx, ptr: WasmPtr<u8, Array>, len: u32) {
    let vec: Vec<u8> = ptr.deref(ctx.memory(0), 0, len).unwrap().into_iter().map(Cell::get).collect();
    let mut slice = vec.as_slice();
    let reader = capnp::serialize::read_message_from_flat_slice(&mut slice, Default::default()).unwrap();
    let foo: foo_capnp::foo::Reader = reader.get_root().unwrap();
    println!("{}", foo.get_s().unwrap());
}

fn main() -> error::Result<()> {
    let wasm_bytes = include_bytes!("../module/target/wasm32-unknown-unknown/debug/module.wasm");

    let import_object = imports! {
        "host" => {
            "foo_print" => func!(foo_print),
        },
    };
    let instance = instantiate(wasm_bytes, &import_object)?;

    let modify_host_side: Func<(WasmPtr<u8, Array>, u32)> = instance.func("modify_host_side")?;
    let memory_alloc: Func<u32, WasmPtr<MemoryBlock>> = instance.func("memory_alloc")?;
    let memory_dealloc: Func<WasmPtr<MemoryBlock>> = instance.func("memory_dealloc")?;
    let memory_ptr: Func<WasmPtr<MemoryBlock>, WasmPtr<u8, Array>> = instance.func("memory_ptr")?;
    let memory_len: Func<WasmPtr<MemoryBlock>, u32> = instance.func("memory_len")?;
    let foo_new: Func<(), WasmPtr<MemoryBlock>> = instance.func("foo_new")?;

    // Host-created structure modified on client
    let mut builder = capnp::message::Builder::new_default();
    {
        let mut foo = builder.init_root::<foo_capnp::foo::Builder>();
        foo.set_s("Created from Host side");
    }
    let mut vec = Vec::new();
    capnp::serialize::write_message(&mut vec, &builder).unwrap();

    // Allocate a memory block to pass our string back into WASM.  We could also have used a static
    // memory block, or a static vector on the host side, or something else of the sort.
    let block = memory_alloc.call(vec.len() as u32)?;
    let block_ptr = memory_ptr.call(block)?;
    for (cell, byte) in block_ptr
        .deref(instance.context().memory(0), 0, vec.len() as u32)
        .unwrap()
        .into_iter()
        .zip(vec.iter())
    {
        cell.set(*byte);
    }
    modify_host_side.call(block_ptr, vec.len() as u32)?;
    memory_dealloc.call(block)?;

    // Client-created structure modified on host and then again on client
    let block = foo_new.call()?;
    let block_ptr = memory_ptr.call(block)?;
    let block_len = memory_len.call(block)?;
    let vec: Vec<u8> = block_ptr.deref(instance.context().memory(0), 0, block_len).unwrap().into_iter().map(Cell::get).collect();
    memory_dealloc.call(block)?;

    let mut slice = vec.as_slice();
    let reader = capnp::serialize::read_message_from_flat_slice(&mut slice, Default::default()).unwrap();
    let foo: foo_capnp::foo::Reader = reader.get_root().unwrap();
    let s = format!("{}: Modified on host", foo.get_s().unwrap());

    let mut builder = capnp::message::Builder::new_default();
    {
        let mut foo = builder.init_root::<foo_capnp::foo::Builder>();
        foo.set_s(&s);
    }
    let mut vec = Vec::new();
    capnp::serialize::write_message(&mut vec, &builder).unwrap();
    let block = memory_alloc.call(vec.len() as u32)?;
    let block_ptr = memory_ptr.call(block)?;
    for (cell, byte) in block_ptr
        .deref(instance.context().memory(0), 0, vec.len() as u32)
        .unwrap()
        .into_iter()
        .zip(vec.iter())
    {
        cell.set(*byte);
    }
    modify_host_side.call(block_ptr, vec.len() as u32)?;
    memory_dealloc.call(block)?;

    Ok(())
}
