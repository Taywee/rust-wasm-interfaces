# Contributing

* Use rustfmt
* Keep examples separated and standalone
* Keep the source code as simple and understandable as possible.  Comments are very welcome, even if they're redundant or obvious, since these are intended to be educational for both me and for any future readers.