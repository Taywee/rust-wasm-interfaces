# shared-capnp-struct

A shared structure, similar to the msgpack one, but using capnproto to avoid
having to copy while deserializing on the WASM side.
You can also avoid these copies using msgpack, but it involves more work, having
to build a struct that may be owned or not (or an entirely referential struct
with the owned data maintained outside of itself).
