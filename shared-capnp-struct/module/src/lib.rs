mod foo_capnp;

use std::slice;

#[link(wasm_import_module = "host")]
extern "C" {
    fn foo_print(ptr: *const u8, len: u32);
}

#[no_mangle]
pub extern "C" fn memory_alloc(size: u32) -> *mut Vec<u8> {
    let mut vec = Vec::with_capacity(size as usize); 
    vec.resize(size as usize, 0);
    Box::into_raw(Box::new(vec))
}

#[no_mangle]
pub extern "C" fn memory_dealloc(block: *mut Vec<u8>) {
    unsafe {
        Box::from_raw(block);
    }
}

#[no_mangle]
pub extern "C" fn memory_ptr(block: *mut Vec<u8>) -> *mut u8 {
    let block = unsafe { block.as_mut().unwrap() };
    block.as_mut_ptr()
}

#[no_mangle]
pub extern "C" fn memory_len(block: *mut Vec<u8>) -> u32 {
    let block = unsafe { block.as_ref().unwrap() };
    block.len() as u32
}

#[no_mangle]
pub extern "C" fn modify_host_side(ptr: *const u8, len: u32) {
    let mut slice = unsafe {
        slice::from_raw_parts(ptr, len as usize)
    };
    let reader = capnp::serialize::read_message_from_flat_slice(&mut slice, Default::default()).unwrap();
    let foo: foo_capnp::foo::Reader = reader.get_root().unwrap();
    let s = format!("{}: plus bit from WASM side", foo.get_s().unwrap());
    let mut builder = capnp::message::Builder::new_default();
    {
        let mut foo = builder.init_root::<foo_capnp::foo::Builder>();
        foo.set_s(&s);
    }
    let mut vec = Vec::new();
    capnp::serialize::write_message(&mut vec, &builder).unwrap();
    unsafe {
        foo_print(vec.as_ptr(), vec.len() as u32);
    }
}

#[no_mangle]
pub extern "C" fn foo_new() -> *mut Vec<u8> {
    let mut builder = capnp::message::Builder::new_default();
    {
        let mut foo = builder.init_root::<foo_capnp::foo::Builder>();
        foo.set_s("Created from the client side");
    }
    let mut vec = Vec::new();
    capnp::serialize::write_message(&mut vec, &builder).unwrap();
    Box::into_raw(Box::new(vec))
}
