use serde::{Serialize, Deserialize};
use std::slice;
use std::str;

#[link(wasm_import_module = "host")]
extern "C" {
    fn foo_print(ptr: *const u8, len: u32);
}

/// A simple structure that stores a string, but it could be any arbitrarily-complex structure
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Foo<'a> {
    s: &'a str,
}

/// Allocate memory using a vector
#[no_mangle]
pub extern "C" fn memory_alloc(size: u32) -> *mut Vec<u8> {
    let mut vec = Vec::with_capacity(size as usize); 
    vec.resize(size as usize, 0);
    Box::into_raw(Box::new(vec))
}

/// drop a vector of memory
#[no_mangle]
pub extern "C" fn memory_dealloc(block: *mut Vec<u8>) {
    unsafe {
        Box::from_raw(block);
    }
}

/// get the data block that the vector contains
#[no_mangle]
pub extern "C" fn memory_ptr(block: *mut Vec<u8>) -> *mut u8 {
    let block = unsafe { block.as_mut().unwrap() };
    block.as_mut_ptr()
}

/// Get the vector's length
#[no_mangle]
pub extern "C" fn memory_len(block: *mut Vec<u8>) -> u32 {
    let block = unsafe { block.as_ref().unwrap() };
    block.len() as u32
}

/// Get a host Foo struct as a msgpack block, deserialize it, change the string, and reserialize
/// and print it
#[no_mangle]
pub extern "C" fn modify_host_side(ptr: *const u8, len: u32) {
    let slice = unsafe {
        slice::from_raw_parts(ptr, len as usize)
    };
    let mut foo: Foo = rmp_serde::from_read_ref(slice).unwrap();
    let s = format!("{}: plus bit from WASM side", foo.s);
    foo.s = &s;
    let vec = rmp_serde::encode::to_vec(&foo).unwrap();
    unsafe {
        foo_print(vec.as_ptr(), vec.len() as u32);
    }
}

/// Make a new Foo struct
#[no_mangle]
pub extern "C" fn foo_new() -> *mut Vec<u8> {
    let foo = Foo {
        s: "Created from Client side",
    };

    let vec = rmp_serde::encode::to_vec(&foo).unwrap();
    Box::into_raw(Box::new(vec))
}
