use serde::{Serialize, Deserialize};
use std::slice;
use std::str;

#[link(wasm_import_module = "host")]
extern "C" {
    fn foo_print(ptr: *const u8, len: u32);
}

// The struct that stores all the data.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Foo {
    s: String,
}

#[no_mangle]
pub extern "C" fn memory_alloc(size: u32) -> *mut Vec<u8> {
    let mut vec = Vec::with_capacity(size as usize); 
    vec.resize(size as usize, 0);
    Box::into_raw(Box::new(vec))
}

#[no_mangle]
pub extern "C" fn memory_dealloc(block: *mut Vec<u8>) {
    unsafe {
        Box::from_raw(block);
    }
}

#[no_mangle]
pub extern "C" fn memory_ptr(block: *mut Vec<u8>) -> *mut u8 {
    let block = unsafe { block.as_mut().unwrap() };
    block.as_mut_ptr()
}

#[no_mangle]
pub extern "C" fn memory_len(block: *mut Vec<u8>) -> u32 {
    let block = unsafe { block.as_ref().unwrap() };
    block.len() as u32
}

#[no_mangle]
pub extern "C" fn modify_host_side(ptr: *const u8, len: u32) {
    let slice = unsafe {
        slice::from_raw_parts(ptr, len as usize)
    };
    let mut foo: Foo = rmp_serde::from_read(slice).unwrap();
    foo.s = format!("{}: plus bit from WASM side", foo.s);
    let vec = rmp_serde::encode::to_vec(&foo).unwrap();
    unsafe {
        foo_print(vec.as_ptr(), vec.len() as u32);
    }
}

#[no_mangle]
pub extern "C" fn foo_new() -> *mut Vec<u8> {
    let foo = Foo {
        s: "Created from Client side".into(),
    };

    let vec = rmp_serde::encode::to_vec(&foo).unwrap();
    Box::into_raw(Box::new(vec))
}
