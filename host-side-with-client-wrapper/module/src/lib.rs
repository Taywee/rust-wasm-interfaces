use std::str;
use std::os::raw::c_void;

#[link(wasm_import_module = "host")]
extern "C" {
    fn users_new() -> u64;
    fn users_len(users: u64) -> u64;
    fn users_free(users: u64);
    fn users_add(users: u64, user: u64);
    fn users_get_user(users: u64, user: u64) -> u64;

    fn user_new() -> u64;
    fn user_free(user: u64);
    fn user_set_name(user: u64, name: *const u8, name_len: u32);
    fn user_get_name(user: u64) -> *mut c_void;

    fn print(s: *const u8, len: u32);
}

pub fn print_str<S: AsRef<str>>(s: S) {
    let s = s.as_ref();
    unsafe {
        print(s.as_ptr(), s.len() as u32);
    }
}

struct Users {
    handle: u64,
}

struct User {
    handle: u64,
    owned: bool,
}

impl Users {
    fn new() -> Users {
        let handle = unsafe {
            users_new()
        };

        Users {
            handle
        }
    }

    fn len(&self) -> usize {
        unsafe {
            users_len(self.handle) as usize
        }
    }

    fn add(&mut self, mut user: User) {
        user.owned = true;
        unsafe {
            users_add(self.handle, user.handle)
        }
    }

    fn get_user(&mut self, user: usize) -> User {
        let handle = unsafe {
            users_get_user(self.handle, user as u64)
        };
        User {
            handle,
            owned: true,
        }
    }
}

impl User {
    fn new() -> User {
        let handle = unsafe {
            user_new()
        };

        User {
            handle,
            owned: false,
        }
    }

    fn get_name(&self) -> String {
        // Allocates a Vec<u8> to pass in, so we can simply take full ownership here.
        let vec = unsafe {
            Box::from_raw(user_get_name(self.handle) as *mut Vec<u8>)
        };
        // We (should) know the value is valid
        String::from_utf8(*vec).unwrap()
    }

    fn set_name<S: AsRef<str>>(&mut self, name: S) {
        let name = name.as_ref();
        unsafe {
            user_set_name(self.handle, name.as_ptr(), name.len() as u32);
        }
    }
}

impl Drop for Users {
    fn drop(&mut self) {
        unsafe {
            users_free(self.handle);
        }
    }
}

impl Drop for User {
    fn drop(&mut self) {
        unsafe {
            if !self.owned {
                user_free(self.handle);
            }
        }
    }
}

#[no_mangle]
pub extern "C" fn main() {
    let mut users = Users::new();
    let mut user = User::new();
    user.set_name("Taylor");
    users.add(user);
    let mut user = User::new();
    user.set_name("Amber");
    users.add(user);
    let mut user = User::new();
    user.set_name("Silas");
    users.add(user);
    let mut user = User::new();
    user.set_name("Liam");
    users.add(user);

    let mut user = users.get_user(2);
    print_str(format!("User count: {}.  Third user name: {}\n", users.len(), user.get_name()));
    print_str(format!("Changing {} name to Batman\n", user.get_name()));
    user.set_name("Batman");
}

#[no_mangle]
pub extern "C" fn memory_alloc(size: u32) -> *mut c_void {
    let mut vec = Vec::with_capacity(size as usize); 
    vec.resize(size as usize, 0);
    Box::into_raw(Box::new(vec)) as *mut c_void
}

#[no_mangle]
pub extern "C" fn memory_ptr(block: *mut Vec<u8>) -> *mut u8 {
    let block = unsafe { block.as_mut().unwrap() };
    block.as_mut_ptr()
}

#[no_mangle]
pub extern "C" fn memory_free(block: *mut Vec<u8>) {
    unsafe {
        Box::from_raw(block);
    }
}
