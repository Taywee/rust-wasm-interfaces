// Import the wasmer runtime so we can use it
use wasmer_runtime::{error, imports, instantiate, Array, Func, WasmPtr, func, Ctx};
use serde::{Serialize, Deserialize};
use std::cell::Cell;

/// Zero-sized struct used as an opaque ptr
#[derive(Copy, Clone)]
pub struct MemoryBlock {
    _unused: [u8; 0],
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Foo {
    s: String,
}

fn foo_print(ctx: &mut Ctx, ptr: WasmPtr<u8, Array>, len: u32) {
    let vec: Vec<u8> = ptr.deref(ctx.memory(0), 0, len).unwrap().into_iter().map(Cell::get).collect();
    let foo: Foo = rmp_serde::from_read(vec.as_slice()).unwrap();
    println!("{}", foo.s);
}

fn main() -> error::Result<()> {
    let wasm_bytes = include_bytes!("../module/target/wasm32-unknown-unknown/debug/module.wasm");

    let import_object = imports! {
        "host" => {
            "foo_print" => func!(foo_print),
        },
    };
    let instance = instantiate(wasm_bytes, &import_object)?;

    let modify_host_side: Func<(WasmPtr<u8, Array>, u32)> = instance.func("modify_host_side")?;
    let memory_alloc: Func<u32, WasmPtr<MemoryBlock>> = instance.func("memory_alloc")?;
    let memory_dealloc: Func<WasmPtr<MemoryBlock>> = instance.func("memory_dealloc")?;
    let memory_ptr: Func<WasmPtr<MemoryBlock>, WasmPtr<u8, Array>> = instance.func("memory_ptr")?;
    let memory_len: Func<WasmPtr<MemoryBlock>, u32> = instance.func("memory_len")?;
    let foo_new: Func<(), WasmPtr<MemoryBlock>> = instance.func("foo_new")?;

    // Host-created structure modified on client
    let foo = Foo {
        s: "Created from Host side".into(),
    };
    let vec = rmp_serde::encode::to_vec(&foo).unwrap();

    // Allocate a memory block to pass our string back into WASM.  We could also have used a static
    // memory block, or a static vector on the host side, or something else of the sort.
    let block = memory_alloc.call(vec.len() as u32)?;
    let block_ptr = memory_ptr.call(block)?;
    for (cell, byte) in block_ptr
        .deref(instance.context().memory(0), 0, vec.len() as u32)
        .unwrap()
        .into_iter()
        .zip(vec.iter())
    {
        cell.set(*byte);
    }
    modify_host_side.call(block_ptr, vec.len() as u32)?;
    memory_dealloc.call(block)?;

    // Client-created structure modified on host and then again on client
    let block = foo_new.call()?;
    let block_ptr = memory_ptr.call(block)?;
    let block_len = memory_len.call(block)?;
    let vec: Vec<u8> = block_ptr.deref(instance.context().memory(0), 0, block_len).unwrap().into_iter().map(Cell::get).collect();
    memory_dealloc.call(block)?;

    let mut foo: Foo = rmp_serde::from_read(vec.as_slice()).unwrap();
    foo.s = format!("{}: Modified on host", foo.s);
    let vec = rmp_serde::encode::to_vec(&foo).unwrap();
    let block = memory_alloc.call(vec.len() as u32)?;
    let block_ptr = memory_ptr.call(block)?;
    for (cell, byte) in block_ptr
        .deref(instance.context().memory(0), 0, vec.len() as u32)
        .unwrap()
        .into_iter()
        .zip(vec.iter())
    {
        cell.set(*byte);
    }
    modify_host_side.call(block_ptr, vec.len() as u32)?;
    memory_dealloc.call(block)?;

    Ok(())
}
