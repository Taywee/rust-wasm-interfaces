use std::slice;
use std::str;

// The struct that stores all the data.
pub struct Foo {
    s: String,
}

#[no_mangle]
pub extern "C" fn foo_new() -> *mut Foo {
    let foo = Foo {
        s: String::from("String set in constructor in WASM client program"),
    };

    Box::into_raw(Box::new(foo))
}

#[no_mangle]
pub extern "C" fn foo_delete(foo: *mut Foo) {
    unsafe {
        // Let Box drop Foo
        Box::from_raw(foo);
    }
}

#[no_mangle]
pub extern "C" fn foo_get_string(foo: *const Foo) -> *const u8 {
    let foo = unsafe { foo.as_ref().unwrap() };
    foo.s.as_ptr()
}

#[no_mangle]
pub extern "C" fn foo_get_string_len(foo: *const Foo) -> u32 {
    let foo = unsafe { foo.as_ref().unwrap() };
    foo.s.len() as u32
}

#[no_mangle]
pub extern "C" fn foo_set_string(foo: *mut Foo, ptr: *const u8, len: u32) {
    let foo = unsafe { foo.as_mut().unwrap() };
    let slice = unsafe { slice::from_raw_parts(ptr, len as usize) };
    let string = str::from_utf8(slice).unwrap();
    foo.s = string.into();
}

#[no_mangle]
pub extern "C" fn memory_alloc(size: u32) -> *mut Vec<u8> {
    let mut vec = Vec::with_capacity(size as usize); 
    vec.resize(size as usize, 0);
    Box::into_raw(Box::new(vec))
}

#[no_mangle]
pub extern "C" fn memory_dealloc(block: *mut Vec<u8>) {
    unsafe {
        Box::from_raw(block);
    }
}

#[no_mangle]
pub extern "C" fn memory_ptr(block: *mut Vec<u8>) -> *mut u8 {
    let block = unsafe { block.as_mut().unwrap() };
    block.as_mut_ptr()
}
