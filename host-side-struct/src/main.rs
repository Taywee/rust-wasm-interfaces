// Import the wasmer runtime so we can use it
use wasmer_runtime::{error, func, imports, instantiate, Array, Ctx, Func, WasmPtr};

// The struct that stores all the data.
struct Foo {
    s: String,
}

fn foo_new() -> u64 {
    let foo = Foo {
        s: String::from("String set in constructor in rust host program"),
    };

    let ptr: *mut Foo = Box::into_raw(Box::new(foo));
    ptr as usize as u64
}

fn foo_delete(foo: u64) {
    unsafe {
        // Let Box drop Foo
        Box::from_raw(foo as usize as *mut Foo)
    };
}

fn foo_get_string(ctx: &mut Ctx, foo: u64, ptr: WasmPtr<u8, Array>) {
    let foo = unsafe { &*(foo as usize as *mut Foo) };
    let memory = ctx.memory(0);
    let block = ptr.deref(memory, 0, foo.s.len() as u32).unwrap();
    for (byte, cell) in foo.s.bytes().zip(block.into_iter()) {
        cell.set(byte);
    }
}

fn foo_get_string_len(foo: u64) -> u64 {
    let foo = unsafe { &*(foo as usize as *mut Foo) };
    foo.s.len() as u64
}

fn foo_set_string(ctx: &mut Ctx, foo: u64, ptr: WasmPtr<u8, Array>, len: u64) {
    let foo = unsafe { &mut *(foo as usize as *mut Foo) };
    let memory = ctx.memory(0);
    let string = ptr.get_utf8_string(memory, len as u32).unwrap();
    foo.s = string.into();
}

fn foo_print(foo: u64) {
    let foo = unsafe { &*(foo as usize as *mut Foo) };
    println!("{}", foo.s);
}

fn main() -> error::Result<()> {
    let wasm_bytes = include_bytes!("../module/target/wasm32-unknown-unknown/debug/module.wasm");

    let import_object = imports! {
        "host" => {
            "foo_new" => func!(foo_new),
            "foo_delete" => func!(foo_delete),
            "foo_get_string" => func!(foo_get_string),
            "foo_get_string_len" => func!(foo_get_string_len),
            "foo_set_string" => func!(foo_set_string),
            "foo_print" => func!(foo_print),
        },
    };
    let instance = instantiate(wasm_bytes, &import_object)?;

    let main: Func = instance.func("main")?;
    main.call()?;

    Ok(())
}
