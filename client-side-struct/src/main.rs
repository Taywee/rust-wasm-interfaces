// Import the wasmer runtime so we can use it
use wasmer_runtime::{error, imports, instantiate, Array, Func, WasmPtr};

/// Zero-sized struct used as an opaque ptr
#[derive(Copy, Clone)]
pub struct Foo {
    _unused: [u8; 0],
}

/// Zero-sized struct used as an opaque ptr
#[derive(Copy, Clone)]
pub struct MemoryBlock {
    _unused: [u8; 0],
}

fn main() -> error::Result<()> {
    let wasm_bytes = include_bytes!("../module/target/wasm32-unknown-unknown/debug/module.wasm");

    let import_object = imports! {};
    let instance = instantiate(wasm_bytes, &import_object)?;

    // Opaque ptr
    let foo_new: Func<(), WasmPtr<Foo>> = instance.func("foo_new")?;
    let foo_delete: Func<WasmPtr<Foo>> = instance.func("foo_delete")?;
    let foo_get_string: Func<WasmPtr<Foo>, WasmPtr<u8, Array>> = instance.func("foo_get_string")?;
    let foo_get_string_len: Func<WasmPtr<Foo>, u32> = instance.func("foo_get_string_len")?;
    let foo_set_string: Func<(WasmPtr<Foo>, WasmPtr<u8, Array>, u32)> =
        instance.func("foo_set_string")?;
    let memory_alloc: Func<u32, WasmPtr<MemoryBlock>> = instance.func("memory_alloc")?;
    let memory_dealloc: Func<WasmPtr<MemoryBlock>> = instance.func("memory_dealloc")?;
    let memory_ptr: Func<WasmPtr<MemoryBlock>, WasmPtr<u8, Array>> = instance.func("memory_ptr")?;

    let foo = foo_new.call()?;
    let ptr = foo_get_string.call(foo)?;
    let len = foo_get_string_len.call(foo)?;

    let string = ptr
        .get_utf8_string(instance.context().memory(0), len)
        .unwrap();
    let new_string = format!("{}: Plus bit from host side", string);

    // Allocate a memory block to pass our string back into WASM.  We could also have used a static
    // memory block, or a static vector on the host side, or something else of the sort.
    let block = memory_alloc.call(new_string.len() as u32)?;
    let block_ptr = memory_ptr.call(block)?;
    for (cell, byte) in block_ptr
        .deref(instance.context().memory(0), 0, new_string.len() as u32)
        .unwrap()
        .into_iter()
        .zip(new_string.bytes())
    {
        cell.set(byte)
    }
    foo_set_string.call(foo, block_ptr, new_string.len() as u32)?;
    memory_dealloc.call(block)?;

    // Now print the string.  We could also expose a "print" function on the host side to push this
    // through.
    let ptr = foo_get_string.call(foo)?;
    let len = foo_get_string_len.call(foo)?;

    let string = ptr
        .get_utf8_string(instance.context().memory(0), len)
        .unwrap();
    println!("{}", string);

    foo_delete.call(foo)?;

    Ok(())
}
