# host-side-struct

This example is a struct created, managed, and allocated on the host side.
It is worked with from the client side via a C-style interface and an opaque
pointer (operated on on the WASM side via a u64, because pointer sizes are
different).

The string is get from the host side by requesting the size of the string in
WASM, and then giving the host a buffer to fill.  We could have also had the
host request a block of memory to fill and give it to WASM, but then the guest
would have to free that as well, which would be some interesting back and forth.

This is probably the most familiar approach to people used to sharing structures
across language boundaries without exposing structure layouts.
