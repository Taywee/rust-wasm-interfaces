# shared-msgpack-struct

This is an example of a shared struct.  The layout may vary across
the two architectures, so the struct is passed as a memory buffer containing a
msgpack-serialized representation of the data.
