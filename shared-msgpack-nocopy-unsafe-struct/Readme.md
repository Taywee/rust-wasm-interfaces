# shared-msgpack-nocopy-struct

This is like shared-msgpack-struct, but using deserializer lifetimes to help
avoid copying, at least on the client side.  As far as I can tell, there's no
way to entirely avoid copying on the host side (because we can't simply safely
get a memory view as a raw u8 slice), but we can avoid a redundant additonal
copy at least.

## Warning

This is an experiment.  As everything involving unsafe, you should only do it if
you really need to, and in this case, you probably don't.
