# Rust WASM Interfaces

Some examples of different ways of interfacing between a Wasmer-based Rust host
program and a contained WASM Rust module.

Each of these should do roughly the same thing, that is:

* Create a structure with a string
* Modify the structure from the other side
* Print out the modified string
* Destroy the structure

These will be as simple as they can be, and will therefore forego conveniences
like droppable wrappers.  Necessary cleanup will be done manually.

I considered making this top-level a workspace, but I don't see much of a gain
for something like this.  Nothing depends on shared versions of anything, and
none of the "dependencies" are true Rust dependencies.

## Building and running examples

Each example has the wasm module as "module".  You must have rust able to target
wasm32-unknown-unknown (should be something like `rustup target add
wasm32-unknown-unknown).  Go into the "module" directory and run `cargo build
--target wasm32-unknown-unknown`.  If all worked, the target directory should
have been built, containing the appropriate wasm module.  You can then go up a
directory and run `cargo run` to run the host application that loads in this
wasm module and runs it.

## Acknowledgement

Though this is likely heavily changed to the point of not having any actual code
in common, the starting point for all of these was the
[wasmerio/wasmer-rust-example](https://github.com/wasmerio/wasmer-rust-example)
repository, and many chunks were also borrowed directly from the tutorial pages
in the [Wasmer Docs](https://docs.wasmer.io/).
