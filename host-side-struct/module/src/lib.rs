use std::str;

#[link(wasm_import_module = "host")]
extern "C" {
    fn foo_new() -> u64;
    fn foo_delete(foo: u64);
    fn foo_get_string(foo: u64, buffer: *mut u8);
    fn foo_get_string_len(foo: u64) -> u64;
    fn foo_set_string(foo: u64, str_ptr: *const u8, str_len: u64);
    fn foo_print(foo: u64);
}

#[no_mangle]
pub extern "C" fn main() {
    unsafe {
        let foo = foo_new();
        let str_len = foo_get_string_len(foo);
        let mut str_buf = Vec::with_capacity(str_len as usize);
        str_buf.resize(str_len as usize, 0);
        foo_get_string(foo, str_buf.as_mut_ptr());

        let s = str::from_utf8_unchecked(&str_buf);
        let new_string = format!("{}: plus wasm bit at end", s);

        foo_set_string(foo, new_string.as_ptr(), new_string.len() as u64);
        foo_print(foo);
        foo_delete(foo);
    }
}
